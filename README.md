# manifests

## CI

[CI documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/ci/README.md)

## Deployment

[Deployment documentation](https://gitlab.com/redhat/edge/ci-cd/manifests/-/blob/main/deployment/README.md)

## Vagrant

The local environment provided by Vagrant is based on CentOS Stream 9.

Supported providers:

* libvirt
* virtualbox


### Starting

```vagrant
# libvirt
vagrant up --provider=libvirt

# virtualbox
vagrant up --provider=virtualbox

# using default provider from an env var
export VAGRANT_DEFAULT_PROVIDER="libvirt"

vagrant up
```

All files of this repository will be synced in `/vagrant` inside the crated virtual machine.

### Destroying

```vagrant
vagrant destroy
```

### Create-Yum-Repo

The create-yum-repo script can be run inside the vagrant VM with the following
commands.

```
make yum-repo-cs8
make yum-repo-cs9
```

By default, the input data is pulled from /vagrant/automotive-sig. If you wish
to use input data from a different location, simply set the TARGET_PROJ variable
to point to the directory containing the desired package_list files. See the
example below.

Note it must be specified as an absolute path.

```
make yum-repo-cs9 TARGET_PROJ=/home/vagrant/my-automotive-sig
```

By default the output yum repo is created in /var/lib/repos.

If you want to change the output directory, set the make variable REPO_DIR like
in the example below.

```
make yum-repo-cs8 REPO_DIR=/tmp/my-repo
```

Note that any existing repositories in the REPO_DIR folder will be deleted by
the script when run as the script requires a clean workspace to output to.

## CODEOWNERS

Specify which people or group to trigger an approval request.

This will ensure the MR to be approved by at least one people
who can give is expertise.

ℹ️ If you want to manage more file types for approval, please open an issue or contribute to the file! 👍

🌐 https://docs.gitlab.com/ee/user/project/code_owners.html

The local environment provided by Vagrant is based on CentOS Stream 9.
