#!/bin/bash
set -euxo pipefail

# following steps as noted on https://hackmd.io/ZbUOkeIXTjaP7XVpTSsrNw?view

# Get OS data.
source /etc/os-release

ID=${ID:-}
ARCH=$(arch)
UUID=${UUID:-local}
CS_VERSION="${CS_VERSION:-8}"
BUCKET=${BUCKET:-}
REGION=${REGION:-}
REPO_URL="https://${BUCKET}.s3.${REGION}.amazonaws.com/${UUID}/cs${CS_VERSION}"
DISK_IMAGE=${DISK_IMAGE:-"image_output/image/disk.img"}
IMAGE_FILE=${IMAGE_FILE:-"/var/lib/libvirt/images/auto-osbuild-cs${CS_VERSION}-${ARCH}-${UUID}.raw"}
# allow for a different manifest file to be built - passed via pipeline parameters
PREPROCESSOR_FILE=${PREPROCESSOR_FILE:-$1}
OSBUILT_FILE=ci/create-osbuild/cs${CS_VERSION}-${ARCH}.mpp.json.built


echo ""
echo "[+] Preprocessing $PREPROCESSOR_FILE"
echo ""

if [[ "${UUID}" != "local" ]]; then
  # install osbuild and osbuild-tools, which contains osbuild-mpp utility
  dnf -y copr enable @osbuild/osbuild
  # TODO as of 12 November 2021 we do not have epel-9 repos, so leaving this as-is: epel-9, for testing
  #SEARCH_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/epel-${CS_VERSION}-\$basearch/'
  #REPLACE_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/centos-stream-'"${CS_VERSION}"'-$basearch/'
  SEARCH_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/epel-8-\$basearch/'
  REPLACE_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/centos-stream-8-$basearch/'
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr\:copr.fedorainfracloud.org\:group_osbuild\:osbuild.repo

  # TODO - this check can be removed when cs8 support is no longer needed
  if [[ "${CS_VERSION}" == "8" ]]; then
    # force python36, to avoid this osbuild's bug: https://github.com/osbuild/osbuild/issues/757
    dnf -y install python36 osbuild osbuild-tools osbuild-ostree
  else
    dnf -y install python3 osbuild osbuild-tools osbuild-ostree
  fi

  # enable neptune copr repo
  dnf -y copr enable pingou/qtappmanager-fedora
  #SEARCH_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${CS_VERSION}-$basearch/'
  #REPLACE_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-'"${CS_VERSION}"'-$basearch/'
  SEARCH_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-8-$basearch/'
  REPLACE_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-8-$basearch/' 
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  # Set the pipeline's yum-repo
  echo "[+] Using the pipeline's yum-repo:"
  echo "repo: ${REPO_URL}"
  osbuild-mpp -D "cs${CS_VERSION}_baseurl=\"${REPO_URL}\"" "$PREPROCESSOR_FILE" "$OSBUILT_FILE"

else
  REVISION="main"

  # enable neptune copr repo
  sudo dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-8-$basearch/'
  REPLACE_PATTERN='baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-8-$basearch/' 
  sudo sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  osbuild-mpp "$PREPROCESSOR_FILE" "$OSBUILT_FILE"
fi

# build the image
sudo osbuild \
	--store osbuild_store \
	--output-directory image_output \
	--export image \
	$OSBUILT_FILE

echo "[+] Moving the generated image"
sudo mkdir -p $(dirname $IMAGE_FILE)
sudo mv $DISK_IMAGE $IMAGE_FILE

# record some details of the input manifest, and disk image in json
sudo cat <<EOF > ${IMAGE_FILE%.*}.json
{
  "preprocessor_file": "${PREPROCESSOR_FILE}",
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "centos_version": "${CS_VERSION}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF

# Clean up
echo "[+] Cleaning up"
sudo rm -fr image_output osbuild_store

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
echo
