import boto3
import os
import pathlib
import pytest
import sys

from botocore.exceptions import ClientError

import main

def test_upload_directory(repodir: str, bucket: str, boto3_session: str) -> bool:
    assert main.upload_directory(repodir, "pipeline-run", bucket) == 0


def test_upload_file(
    s3_client: boto3.client, repodir: str, file: pathlib.Path, s3_resource: boto3.resources
) -> bool:
    s3_client.create_bucket(Bucket="bucket")
    main.upload_file(file, s3_client, "bucket", "prefix", repodir)

    ret = 1
    try:
        s3_resource.Object("bucket", "prefix/myfile.txt").load()
        ret = 0
    except ClientError as e:
        ret = 1
    assert ret == 0

def test_download_packages_cs8(
    repodir: str,
    supported_arch: str,
    repositories: list,
    mirror_list: list,
) -> bool:
    main.download_packages(
        "./tests/resources/manifest_repo/package_list", "cs8", repodir, supported_arch, repositories, mirror_list["cs8"]
    )==0

    for repo in repositories:
        assert os.path.exists(f"{repodir}/{repo}/{supported_arch}/os") == True

def test_download_packages_cs9(
    repodir: str,
    supported_arch: str,
    repositories: list,
    mirror_list: list,
) -> bool:
    main.download_packages(
        "./tests/resources/manifest_repo/package_list", "cs9", repodir, supported_arch, repositories, mirror_list["cs9"]
    )==0

    for repo in repositories:
        assert os.path.exists(f"{repodir}/{repo}/{supported_arch}/os") == True
