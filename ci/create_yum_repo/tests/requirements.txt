boto3==1.18.26
botocore==1.21.63
jinja2==3.0.1
moto==2.2.10
pytest==6.2.4
