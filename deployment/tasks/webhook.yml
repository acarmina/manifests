---
- name: A-Team Pipeline | webhook | TriggerTemplate
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerTemplate
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: git-repo-url
            description: The git repository url
          - name: git-revision
            description: The git revision
            default: main
          - name: merge-request-id
            description: The Merge Request id
          - name: source-project-id
            description: The source project id
          - name: target-project-id
            description: The target project id
        resourcetemplates:
          - apiVersion: tekton.dev/v1beta1
            kind: PipelineRun
            metadata:
              generateName: a-team-
              labels:
                a-team/merge_request_id: $(tt.params.merge-request-id)
            spec:
              serviceAccountName: pipeline
              timeout: 2h0m0s
              workspaces:
                - name: shared-workspace
                  volumeClaimTemplate:
                    spec:
                      accessModes:
                        - ReadWriteOnce
                      storageClassName: gp2-csi
                      resources:
                        requests:
                          storage: 1Gi
              podTemplate:
                securityContext:
                  fsGroup: 1001000000
              pipelineRef:
                name: a-team
              params:
                - name: git-repo-url
                  value: $(tt.params.git-repo-url)
                - name: git-revision
                  value: $(tt.params.git-revision)
                - name: merge-request-id
                  value: $(tt.params.merge-request-id)
                - name: source-project-id
                  value: $(tt.params.source-project-id)
                - name: target-project-id
                  value: $(tt.params.target-project-id)

- name: A-Team Pipeline | webhook | TriggerBinding
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerBinding
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: git-repo-url
            value: $(body.object_attributes.source.git_http_url)
          - name: git-revision
            value: $(body.object_attributes.last_commit.id)
          - name: merge-request-id
            value: $(body.object_attributes.iid)
          - name: source-project-id
            value: $(body.object_attributes.source.id)
          - name: target-project-id
            value: $(body.object_attributes.target.id)

- name: A-Team Pipeline | webhook | Trigger
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: Trigger
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        serviceAccountName: pipeline
        interceptors:
          - ref:
              name: cel
            params:
              - name: filter
                value: |
                  header.match('X-Gitlab-Event', 'Merge Request Hook') &&
                  !body.object_attributes.title.contains("WIP:") &&
                    (
                      body.object_attributes.action == "open" ||
                      body.object_attributes.action == "reopen" ||
                      (has(body.changes.title) && body.changes.title.previous.contains("WIP:")) ||
                      (has(body.changes.title) && body.changes.title.previous.contains("Draft:")) ||
                      (body.object_attributes.action == "update" && has(body.object_attributes.oldrev))
                    )
              - name: overlays
                value:
                  - key: project_id
                    expression: body.project.id
                  - key: merge_request_id
                    expression: body.object_attributes.id
                  - key: url
                    expression: body.project.git_ssh_url
                  - key: ref
                    expression: body.object_attributes.last_commit.id
        bindings:
          - ref: a-team
        template:
          ref: a-team

- name: A-Team Pipeline | webhook | EventListener
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: EventListener
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        serviceAccountName: pipeline
        triggers:
          - triggerRef: a-team

- name: A-Team Pipeline | webhook | Service
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        ports:
          - name: http-listener
            protocol: TCP
            port: 8080
            targetPort: 8000
        selector:
          eventlistener: a-team

- name: A-Team Pipeline | webhook | Route
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Route
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        to:
          kind: Service
          name: a-team
        port:
          targetPort: http-listener
  register: route_response

- name: A-Team Pipeline | k8s | Set hook url
  ansible.builtin.set_fact:
    hook_url: "http://{{ route_response.result.spec.host }}"
