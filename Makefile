.PHONY: up tmt-tests
.DEFAULT_GOAL := up

ifneq ($(VAGRANT_DEFAULT_PROVIDER),$(filter $(VAGRANT_DEFAULT_PROVIDER),\
		libvirt virtualbox))
VAGRANT_DEFAULT_PROVIDER := libvirt
endif

# start virtual environment
up:
	vagrant up --provider=$(VAGRANT_DEFAULT_PROVIDER)
	# ssh in the virtual environment
	vagrant ssh

# destroying virtual environment
destroy:
	vagrant -f destroy

tmt-tests:
	tmt run -a -vvv \
	provision --how local \
	plans --name /ci/create-osbuild \
	tests --filter tag:-aws

yum-repo-cs8:
	$(MAKE) -C ci yum-repo-cs8

yum-repo-cs9:
	$(MAKE) -C ci yum-repo-cs9
